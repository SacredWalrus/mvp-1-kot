using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChestController : MonoBehaviour
{
    [Header("General")]
    public int key_need;

    [Header("Other")]   
    public ParticleSystem vfx_money;
    public Animator anim;

    private bool isOpen = false;

    [HideInInspector]
    public int curr_key_count;    


    public void ResetChest()
    {
        anim.SetTrigger("reset");
        curr_key_count = 0;
    }

    public void KeyPickUp()
    {
        curr_key_count++;

        if (curr_key_count == key_need)
        {
            anim.SetTrigger("jumping");
        }
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Player" && curr_key_count == key_need && !isOpen)
        {
            isOpen = true;
            OpenChest();
        }
    }

    void OpenChest()
    {
        anim.SetTrigger("open");
        vfx_money.Play();

        GameplayController._playerIsStopped = true;

        GameObject.Find("Floating Joystick").SetActive(false);
        GameObject.Find("Player").GetComponent<PlayerDash>().StoppedPlayer();
        Camera.main.GetComponent<CameraController>().EndLevel();

        GameObject.Find("GameUI").GetComponent<LevelUIController>().Win();
    }
}
