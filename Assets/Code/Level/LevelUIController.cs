using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class LevelUIController : MonoBehaviour
{
    [Header("HP Bar")]
    public Image hp_bar_fill;
    public TMP_Text t_hp;
    private float curr_hp;
    private float max_hp;
    public Image img_star_2, img_star_3;

    [Header("Key Bar")]
    public TMP_Text t_key;
    private int curr_key;
    private int max_key;

    [Header("Start UI")]
    public GameObject panel_start_ui;
    public TMP_Text t_tap_to_play;
    public bool isTextAnimate = false;
    public float speed_text_anim = 0;

    [Header("Win UI")]
    public GameObject win_canvas;
    public Image img_win_star_1, img_win_star_2, img_win_star_3;
    public TMP_Text t_win_money;
    private int stars_count;

    [Header("But Back")]
    public GameObject but_back;


    public void Start()
    {
        max_hp = GameObject.Find("Player").GetComponent<PlayerController>().max_hp;

        but_back.SetActive(true);
        win_canvas.SetActive(false);
        panel_start_ui.SetActive(true);
        isTextAnimate = true;
        StartCoroutine(StartUIEnum());
    }

    public void Update()
    {
        //HP Bar
        curr_hp = GameObject.Find("Player").GetComponent<PlayerController>().curr_hp;
        hp_bar_fill.fillAmount = curr_hp / max_hp;
        //t_hp.text = curr_hp + "/" + max_hp;

        //Key Bar
        curr_key = GameObject.Find("Chest").GetComponent<ChestController>().curr_key_count;
        max_key = GameObject.Find("Chest").GetComponent<ChestController>().key_need;
        t_key.text = curr_key + "/" + max_key;

        if (isTextAnimate)
        {
            t_tap_to_play.fontSizeMax = t_tap_to_play.fontSizeMax + speed_text_anim;
        }

        StarsView();
    }

    void StarsView()
    {
        if (curr_hp > 9)
        {
            img_star_2.color = new Vector4(1, 1, 1, 1);
            img_star_3.color = new Vector4(1, 1, 1, 1);
        }

        if (curr_hp > 5 && curr_hp <= 9)
        {
            img_star_2.color = new Vector4(1, 1, 1, 1);
            img_star_3.color = new Vector4(1, 1, 1, 0.17f);
        }

        if (curr_hp <= 5)
        {
            img_star_2.color = new Vector4(1, 1, 1, 0.17f);
            img_star_3.color = new Vector4(1, 1, 1, 0.17f);
        }
    }    

    public void Win()
    {
        win_canvas.SetActive(true);

        if (curr_hp > 9)
        {
            img_win_star_2.color = new Vector4(1, 1, 1, 1);
            img_win_star_3.color = new Vector4(1, 1, 1, 1);
            stars_count = 3;
        }

        if (curr_hp > 5 && curr_hp <= 9)
        {
            img_win_star_2.color = new Vector4(1, 1, 1, 1);
            img_win_star_3.color = new Vector4(1, 1, 1, 0.17f);
            stars_count = 2;
        }

        if (curr_hp <= 5)
        {
            img_win_star_2.color = new Vector4(1, 1, 1, 0.17f);
            img_win_star_3.color = new Vector4(1, 1, 1, 0.17f);
            stars_count = 1;
        }

        t_win_money.text = "" + GameObject.Find("GameController").GetComponent<LevelController>().level_money_reward;
    }

    public void But_Claim()
    {
        PlayerPrefs.SetInt("level_" + GameObject.Find("GameController").GetComponent<LevelController>().level_num + "_stars_count", stars_count);
        PlayerPrefs.SetInt("player_money", PlayerPrefs.GetInt("player_money") + GameObject.Find("GameController").GetComponent<LevelController>().level_money_reward);

        PlayerPrefs.SetString("level_" + GameObject.Find("GameController").GetComponent<LevelController>().level_num + "_success", "true");
        PlayerPrefs.SetString("level_" + (GameObject.Find("GameController").GetComponent<LevelController>().level_num + 1) + "_unlock", "true");

        SceneTransition.SwithToScene("LevelSelect");
    }

    public void But_Back()
    {
        SceneTransition.SwithToScene("LevelSelect");
    }

    public void StartGame()
    {
        panel_start_ui.SetActive(false);
        StopCoroutine(StartUIEnum());
        isTextAnimate = false;
    }

    IEnumerator StartUIEnum()
    {
        speed_text_anim = Time.deltaTime * 30;
        yield return new WaitForSeconds(1);

        speed_text_anim = -Time.deltaTime * 30;
        yield return new WaitForSeconds(1);
        

        StartCoroutine(StartUIEnum());
    }
}
