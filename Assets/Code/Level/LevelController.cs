using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelController : MonoBehaviour
{
    [Header("General")]
    public int level_num;
    public int level_money_reward;
    private GameObject player;
    public Vector3 player_pos;
    public Vector3 player_rot;
    public int lose_count;

    [Header("Lose UI")]
    public Image img_lose_panel;

    [Header("Chest")]
    public GameObject obj_chest;
    public List<GameObject> obj_keys;


    public void Start()
    {
        lose_count = 0;
        img_lose_panel.gameObject.SetActive(false);
        player = GameObject.Find("Player");
    }

    public void Lose()
    {
        img_lose_panel.gameObject.SetActive(true);
        img_lose_panel.color = new Vector4(0, 0, 0, 0);

        StartCoroutine(LoseEnum());
    }

    IEnumerator LoseEnum()
    {
        yield return new WaitForSeconds(0);
        img_lose_panel.color = new Vector4(0, 0, 0, img_lose_panel.color.a + 0.03f);

        if (img_lose_panel.color.a < 1)
        {
            StartCoroutine(LoseEnum());
        }
        else
        {
            player.transform.position = player_pos;
            player.transform.localEulerAngles = player_rot;
            lose_count += 1;
            GameplayController._playerIsStopped = false;

            obj_chest.GetComponent<ChestController>().ResetChest();

            foreach (GameObject _key in obj_keys)
            {
                _key.SetActive(true);
                _key.GetComponent<KeyController>().ResetKey();
            }


            yield return new WaitForSeconds(0.3f);
            GameObject.Find("GameUI").GetComponent<LevelUIController>().but_back.SetActive(true);
            img_lose_panel.gameObject.SetActive(false);
        }
    }
}
