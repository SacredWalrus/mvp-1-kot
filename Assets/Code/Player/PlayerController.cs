using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PlayerController : MonoBehaviour
{
    public float curr_hp;
    public float max_hp;

    bool dont_hit = false;  //���� true �� �������� ����
    public float dont_hit_timer;  //����� �� ��������� �����

    public List<GameObject> mass_skins;
    public List<SkinnedMeshRenderer> mass_body;
    public List<MeshRenderer> mass_helmet;

    public void Start()
    {
        ChangeSkin();

        curr_hp = max_hp;
        GameplayController._playerIsStopped = false;
        Application.targetFrameRate = 60;
    }

    void ChangeSkin()
    {
        foreach (GameObject _skin in mass_skins)
        {
            _skin.SetActive(false);
        }

        int active_skin = PlayerPrefs.GetInt("active_skin");

        mass_skins[active_skin].SetActive(true);
        body = mass_body[active_skin];
    }

    public void StartChange()
    {
        gameObject.GetComponent<PlayerDash>().enabled = true;
    }

    public void But_Dont_HIT()
    {
        dont_hit = !dont_hit;
    }

    public void Update()
    {
        
    }

    public void Hit(float _dmg)
    {
        if (!dont_hit)
        {
            if (curr_hp > 2)
                curr_hp -= _dmg;

            GameObject.Find("GameController").GetComponent<LevelController>().Lose();
            GameplayController._playerIsStopped = true;
            GameObject.Find("Player").GetComponent<PlayerDash>().StoppedPlayer();
            StartCoroutine(Dont_Hit());
        }
    }

    IEnumerator Dont_Hit()
    {
        StartCoroutine(HitEnum());
        dont_hit = true;
        yield return new WaitForSeconds(dont_hit_timer);
        dont_hit = false;
    }

    public Material hit_mat_1, hit_mat_2;
    public Material head_mat, body_mat;
    private SkinnedMeshRenderer body;

    public IEnumerator HitEnum()
    {
        //On 1
        body.material = hit_mat_1;

        yield return new WaitForSeconds(0.05f);

        //Off
        body.material = body_mat;

        yield return new WaitForSeconds(0.05f);

        //On 2
        body.material = hit_mat_2;

        yield return new WaitForSeconds(0.05f);

        body.material = body_mat;
    }
}
