using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class PlayerDash : MonoBehaviour
{
    Rigidbody rb;
    public float force = 16f;
    public float slow_force = 5;

    public Joystick joy;

    bool change_dir = false;

    bool time_slow = true;

    public GameObject vfx_player_bump;

    public bool isStopped;


    public void Start()
    {
        rb = GetComponent<Rigidbody>();
        joy = GameObject.Find("Floating Joystick").GetComponent<Joystick>();

        Time.timeScale = 1f;
        Time.fixedDeltaTime = Time.timeScale * 0.02f;

        time_slow = true;
    }

    public void Update()
    {       
        if (!GameplayController._playerIsStopped)
        {
            if (Input.GetMouseButton(0))
            {
                change_dir = true;

                if (time_slow)
                {
                    Time.timeScale = 0.2f;
                    Time.fixedDeltaTime = Time.timeScale * 0.02f;
                }

                Vector3 moveDirection = new Vector3(joy.Horizontal, 0, joy.Vertical);

                transform.forward = moveDirection;
                transform.localEulerAngles = new Vector3(transform.localEulerAngles.x, transform.localEulerAngles.y - 45f, transform.localEulerAngles.z);
            }

            if (Input.GetMouseButtonUp(0))
            {
                change_dir = false;
                Time.timeScale = 1f;
                Time.fixedDeltaTime = Time.timeScale * 0.02f;

                rb.velocity = new Vector3(0, 0, 0);
                rb.AddForce(transform.forward * force, ForceMode.Impulse);
            }

            if (!change_dir)
            {
                if (rb.velocity.magnitude < force)
                {
                    rb.velocity = rb.velocity.normalized * force;
                }

                if (rb.velocity.magnitude > force)
                {
                    rb.velocity = rb.velocity.normalized * force;
                }

                Vector3 newMoveDirection = rb.velocity;

                transform.forward = newMoveDirection;
            }

            if (change_dir && !time_slow)
            {
                if (rb.velocity.magnitude < slow_force)
                {
                    rb.velocity = rb.velocity.normalized * slow_force;
                }

                if (rb.velocity.magnitude > slow_force)
                {
                    rb.velocity = rb.velocity.normalized * slow_force;
                }
            }
        }
    }    

    public void StoppedPlayer()
    {
        change_dir = false;
        Time.timeScale = 1f;
        Time.fixedDeltaTime = Time.timeScale * 0.02f;
        rb.velocity = new Vector3(0, 0, 0);
    }

    #region Collisions

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "wall")
        {
            Sequence _scale = DOTween.Sequence();

            _scale.Append(transform.DOScaleZ(0.5f, 0.15f));
            _scale.Append(transform.DOScaleZ(0.65f, 0.15f));
        }
    }

    void OnCollisionEnter(Collision collision)
    {
        GameObject _vfx = Instantiate(vfx_player_bump, collision.contacts[0].point, transform.rotation);
        Destroy(_vfx, 1);
    }

    #endregion
}
