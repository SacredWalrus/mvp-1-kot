using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerTrajectory : MonoBehaviour
{
    public GameObject reflection_obj;
    public Transform target_reflection;

    public LineRenderer line_1, line_2;
    public Transform line_2_pos, line_2_pos2;

    public LayerMask trajectory_layer;


    public void Update()
    {
        if (!GameplayController._playerIsStopped)
        {
            if (Input.GetMouseButton(0))
            {
                ReflectionView();
            }

            if (Input.GetMouseButtonUp(0))
            {
                ReflectionOff();
            }
        }
        else
        {
            ReflectionOff();
        }        
    }

    void ReflectionView()
    {
        reflection_obj.SetActive(true);
        line_1.enabled = true;
        line_2.enabled = true;

        RaycastHit hit;

        if (Physics.Raycast(target_reflection.position, transform.TransformDirection(Vector3.forward), out hit, Mathf.Infinity, trajectory_layer))
        {
            reflection_obj.transform.position = hit.point;
        }


        Vector3 newDirection = Vector3.Reflect(transform.forward, hit.normal);
        newDirection = new Vector3(
            -newDirection.x,
            0,
            -newDirection.z
        );
        reflection_obj.transform.rotation = Quaternion.LookRotation(newDirection);

        line_1.SetPosition(0, new Vector3(transform.position.x, 0.1f, transform.position.z));
        line_1.SetPosition(1, new Vector3(hit.point.x, 0.1f, hit.point.z));

        RaycastHit hit2;
        Physics.Raycast(reflection_obj.transform.position, reflection_obj.transform.forward, out hit2, 5, trajectory_layer);

        if (hit.point != null)
            line_2_pos2.transform.position = hit2.point;



        Vector3 new_pos = transform.TransformPoint(line_2_pos.position);

        line_2.SetPosition(0, new Vector3(hit.point.x, 0.1f, hit.point.z));
        //line_2.SetPosition(1, new Vector3(line_2_pos2.position.x, 0.1f, line_2_pos2.position.z));
        line_2.SetPosition(1, new Vector3(line_2_pos.position.x, 0.1f, line_2_pos.position.z));
    }

    void ReflectionOff()
    {
        reflection_obj.SetActive(false);
        line_1.enabled = false;
        line_2.enabled = false;
    }
}
