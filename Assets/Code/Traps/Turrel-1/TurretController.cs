using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretController : MonoBehaviour
{
    [Header("General")]    
    public GameObject gun_obj;

    [Header("Shoting")]
    public GameObject bullet_obj;
    public Transform bullet_spawn_pos;
    public float shoot_speed;
    public float bullet_speed;
    public float damage;

    [Header("Rotate Settings")]
    public float single_step;
    public float max_delta;

    [Header("Other")]    
    private GameObject target;


    public void Start()
    {
        target = GameObject.Find("Player");

        StartCoroutine(Shoting());
    }

    public void Update()
    {
        Rotation();
    }

    void Rotation()
    {
        Vector3 targetDirection = target.transform.position - gun_obj.transform.position;

        // The step size is equal to speed times frame time.
        float singleStep = single_step;

        // Rotate the forward vector towards the target direction by one step
        Vector3 newDirection = Vector3.RotateTowards(gun_obj.transform.forward, targetDirection, singleStep, max_delta);

        // Calculate a rotation a step closer to the target and applies rotation to this object
        gun_obj.transform.rotation = Quaternion.LookRotation(newDirection);

        gun_obj.transform.eulerAngles = new Vector3(0, gun_obj.transform.eulerAngles.y, 0);
    }

    IEnumerator Shoting()
    {
        yield return new WaitForSeconds(shoot_speed);
        Shot();

        StartCoroutine(Shoting());
    }

    void Shot()
    {
        GameObject _bullet = Instantiate(bullet_obj, bullet_spawn_pos.position, gun_obj.transform.rotation);

        _bullet.GetComponent<BulletController>().speed = bullet_speed;
        _bullet.GetComponent<HPMinus>().dmg = damage;
    }
}
