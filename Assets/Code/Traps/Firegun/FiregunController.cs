using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FiregunController : MonoBehaviour
{
    [Header("General")]    
    public float damage;
    public float pause_speed;
    public float attack_speed;

    [Header("Object")]
    public ParticleSystem vfx_fire;
    public GameObject obj_collider;

    [Header("Other")]
    public HPMinus damage_script;


    public void Start()
    {
        obj_collider.SetActive(false);

        var vfx_main = vfx_fire.main;
        vfx_main.duration = attack_speed;

        damage_script.dmg = damage;

        StartCoroutine(AttackEnum());
    }

    IEnumerator AttackEnum()
    {
        yield return new WaitForSeconds(pause_speed);

        obj_collider.SetActive(true);
        vfx_fire.Play();

        yield return new WaitForSeconds(attack_speed);

        obj_collider.SetActive(false);

        StartCoroutine(AttackEnum());
    }
}
