using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class MovingWallController : MonoBehaviour
{
    [Header("General")]
    public GameObject wall_obj;
    public float speed;

    private int curr_target, next_target;

    [Header("Navigation")]
    public List<Transform> targets;


    public void Start()
    {
        curr_target = 0;
        next_target = targets.Count;

        WallMove();
    }

    void WallMove()
    {
        if (curr_target + 1 >= targets.Count)
            curr_target = 0;
        else curr_target += 1;

        if (next_target + 1 >= targets.Count)
            next_target = 0;
        else next_target += 1;

        float _speed;
        _speed = Vector3.Distance(targets[next_target].position, targets[curr_target].position) * speed;

        wall_obj.transform.DOMove(targets[curr_target].position, _speed).SetEase(Ease.Linear).OnComplete(WallMove);
    }
}
