using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class CameraController : MonoBehaviour
{
    public Transform target;

    public float smoothSpeed = 0.125f;
    public Vector3 offset;

    public float min_z;
    Vector3 smoothedPosition;
    Vector3 desiredPosition;

    public bool player_active;

    public bool follow;


    void Start()
    {
        target = GameObject.Find("Player").transform;

        desiredPosition = target.position + offset;

        transform.position = desiredPosition;

        smoothedPosition = Vector3.Lerp(transform.position, desiredPosition, smoothSpeed);

        transform.position = smoothedPosition;

        player_active = true;
        follow = true;
    }

    public void FixedUpdate()
    {
        if (follow)
        {
            desiredPosition = target.position + offset;
            smoothedPosition = Vector3.Lerp(transform.position, desiredPosition, smoothSpeed);

            transform.position = smoothedPosition;
        }
    }

    public void FastFollow()
    {
        desiredPosition = target.position + offset;

        transform.position = desiredPosition;

        smoothedPosition = Vector3.Lerp(transform.position, desiredPosition, smoothSpeed);

        transform.position = smoothedPosition;
        follow = true;
    }

    public void EndLevel()
    {
        transform.GetComponent<Camera>().DOFieldOfView(10, 0.75f).SetEase(Ease.OutBack);
        offset = new Vector3(offset.x, offset.y + 0.7f, offset.z);
    }
}
