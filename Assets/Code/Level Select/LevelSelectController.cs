using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelSelectController : MonoBehaviour
{
    [Header("General")]
    public List<GameObject> obj_level;
    public List<string> level_names;
    public RectTransform content;

    public GameObject panel_shop;


    public void Start()
    {
        content.localPosition = new Vector3(0, 5325.514f, 0);
    }

    public void But_PlayLevel(int level_num)
    {
        if (PlayerPrefs.GetString("level_" + (level_num + 1) + "_unlock") == "true")
            SceneTransition.SwithToScene(level_names[level_num]);
    }

    public void But_Shop()
    {
        panel_shop.SetActive(true);
    }
}
